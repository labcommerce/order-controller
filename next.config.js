/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
}

module.exports = nextConfig
module.exports.env = {
    SOCKET_DOMAIN: 'http://localhost:8083',
    API_DOMAIN: 'http://localhost:8083/api/sdm',
}

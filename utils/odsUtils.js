export function getToday() {


    const cancelDate = new Date();
    const year = cancelDate.getFullYear();
    const month = cancelDate.getMonth() + 1;
    const date = cancelDate.getDate()
    const hour = cancelDate.getHours();
    const min = cancelDate.getMinutes()
    const sec = cancelDate.getSeconds();
    // console.log('year : ' + year);
    // console.log('month : ' + month);
    // console.log('date : ' + date);
    const toDate = year + "-" + (month < 10 ? "0" + month : month) + "-" + (date < 10 ? "0" + date : date) + " "
        + (hour < 10 ? "0" + hour : hour) + ":"
        + (min < 10 ? "0" + min : min) + ":"
        + (sec < 10 ? "0" + sec : sec);
    // console.log('toDate : ' + toDate);

    return toDate;

}

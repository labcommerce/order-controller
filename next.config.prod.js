/** @type {import('next').NextConfig} */
const nextConfig = {
    reactStrictMode: false,
}

module.exports = nextConfig
module.exports.env = {
    SOCKET_DOMAIN: 'https://sdm-api.el-bigs.com',
    API_DOMAIN: 'https://sdm-api.el-bigs.com/api/sdm',
}

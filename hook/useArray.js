import {useCallback, useState} from 'react';


const useArray = (init = []) => {

  const [list, setList] = useState(init);

  const push = (item) => {
    setList(list => [...list, item]);
  }
  const remove = (value, key) => {
    let tempList = list.filter(data => data[key] !== value);
    setList(tempList);
    return tempList ? tempList.length : 0;
  }
  const update = (objectKey, item) => {
    setList(
        list.map(data =>
            data.objectKey === objectKey ? {...item} : data
        )
    );
  }
  const get = (value, key) => {
    let tempList = list.filter(data => data[key] == value);
    if(tempList && tempList.length > 0){
      return {...tempList[0]}
    }else{
      return null;
    }
  }
  const swap = (fromIdx, toIdx) => {
    let tempList = [...list];
    if (toIdx < 0) {
      return;
    }
    if (toIdx > tempList.length) {
      return;
    }
    const item = tempList.splice(fromIdx, 1);
    tempList.splice(toIdx, 0, item[0]);
    setList([...tempList]);
  }
  return [list, setList, push, remove, update, get, swap];
};

export default useArray;


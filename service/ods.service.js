export function getAudioInfo(filename) {
    const alramAudioMap = {
        none: {name: "사용안함", src: null}
        , Doorbell1: {no : '1', src: '/audio/Doorbell1.wav', name: '알림음1', fileName: 'Doorbell1.wav'}
        , Doorbell2: {no : '2', src: '/audio/Doorbell2.wav', name: '알림음2', fileName: 'Doorbell2.wav'}
        , Doorbell3: {no : '3', src: '/audio/Doorbell3.wav', name: '알림음3', fileName: 'Doorbell3.wav'}
        , Doorbell4: {no : '4', src: '/audio/Doorbell4.wav', name: '알림음4', fileName: 'Doorbell4.wav'}
    };

    return alramAudioMap[filename];

}

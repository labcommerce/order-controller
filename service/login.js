import Cookies from 'js-cookie';

export function getLoginInfo() {
  const sdmUser = Cookies.get('sdm_user');
  if(sdmUser){
    return JSON.parse(sdmUser);
  }else{
    return null;
  }
}
export function createCookie(key, value) {
  return Cookies.set(key, value);
}

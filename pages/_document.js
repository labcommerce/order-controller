import Document, {Html, Head, Main, NextScript} from 'next/document';
import React from "react";

class RootDocument extends Document {
    render() {
        return (
            <Html>
                <Head>
                    <meta httpEquiv="X-UA-Compatible" content="IE=edge"/>
                    <meta charSet="UTF-8"/>
                    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0"/>
                    <meta name="format-detection" content="telephone=no, address=no, email=no"/>
                    <meta name="Keywords" content=""/>
                    <meta name="Description" content=""/>
                    <title>ODS</title>
                    <link rel="preload" href="/font/Pretendard-Regular.woff2" as="font" type="font/woff2"
                          crossOrigin/>
                    <link rel="preload" href="/font/Pretendard-Medium.woff2" as="font" type="font/woff2"
                          crossOrigin/>
                    <link rel="preload" href="/font/Pretendard-SemiBold.woff2" as="font" type="font/woff2"
                          crossOrigin/>
                    <link rel="preload" href="/font/Pretendard-Bold.woff2" as="font" type="font/woff2"
                          crossOrigin/>
                </Head>
                <body>
                <Main/>
                <NextScript/>
                </body>
            </Html>
        );
    }
}

export default RootDocument;

import {useRef, useState} from "react";
import axios from "axios";
import useArray from "../hook/useArray";
import {getToday} from "../utils/odsUtils";


export default function Test() {

    const inputRef = useRef(null);
    const cancelRef = useRef(null);

    const [orderList,setOrderList, pushOrder, removeOrder, update] = useArray([]);

    const [orderNum , setOrderNum] = useState(1);

    const uuidv4 = () => {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (Math.random() * 16) | 0,
                v = c == 'x' ? r : (r & 0x3) | 0x8;
            return v.toString(16);
        });
    };

    const createOrder = () => {
        const posShopId = inputRef.current.value;

        setOrderNum(orderNum + 1);
        axios.get(`${process.env.API_DOMAIN}/random-order?orderNum=${orderNum}&posShopId=${posShopId}`).then(res => {
            // console.log(res.data);
            pushOrder({...res.data, objectKey : uuidv4()});
        })
    }

    const cancelOrder = (data) => {
        const toDate = getToday();
        axios.post(`${process.env.API_DOMAIN}/cancel`, {cancelDate : toDate, posShopId : data.posShopId, orderNo: data.orderNo}).then(res => {
            // console.log(res.data);
            // pushOrder({...res.data, objectKey : uuidv4();
            update(data.objectKey, {...data, status : 'cancel'});
        })
    }


    return <>

        <div style={{marginTop: "100px"}}>
            <span>shopId : </span><input defaultValue={"67"} name="" ref={inputRef}/>
            <button style={{
                marginLeft: "20px",
                borderStyle: "solid",
                borderWidth: "1px",
                boxSizing: "border-box",
                fontSize: "20px"
            }} onClick={createOrder}><b>랜덤주문 생성</b></button>
            {orderList.map(data => {
                // console.log("data : ",  data)
                return <><li style={{marginLeft:"10px"}}> {data.posShopId} / {data.orderNo}/ {data.status=='new' ? '주문신청' : data.status=='cancel' ? '취소완료' : ''}
                    {data.status =='new' && <button onClick={()=>{cancelOrder(data)}} style={{ marginLeft:"10px"}}>주문취소</button>}</li></>
            })}
        </div>
{/*        <div style={{marginTop: "50px"}}>
            <span>order code : </span><input defaultValue={""} name="" ref={cancelRef}/>
            <button style={{
                marginLeft: "20px",
                borderStyle: "solid",
                borderWidth: "1px",
                boxSizing: "border-box",
                fontSize: "20px"
            }} onClick={cancelOrder}><b>주문취소</b></button>
        </div>*/}
    </>
}

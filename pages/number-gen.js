import {useEffect, useRef, useState} from "react";
import useArray from "../hook/useArray";
import Stomp from "stompjs";
import SockJS from "sockjs-client";
import {getLoginInfo} from "../service/login";
import {useRouter} from "next/router";


export default function NumberGen(){

    const router = useRouter();

    const [callList, setCallList, pushCall, removeCall, updateCall] = useArray([]);
    const [callMsg , setCallMsg] = useState(null);
    const inputRef = useRef();

    const [shopId, setShopId] = useState(null);
    const [networkStatus, setNetworkStatus] = useState('try');
    const endpoint = `${process.env.SOCKET_DOMAIN}/ws`;
    const stompRef = useRef();

    const call = () => {

        //

        if(!inputRef.current.value){
            alert('번호를 입력해 주세요');
            return;
        }
        const callNum = Number(inputRef.current.value);

        for (let number of callList) {
            if (number == callNum) {
                inputRef.current.value = "";
                return;
            }
        }

        pushCall(callNum);
        setCallMsg(callNum);

        inputRef.current.value = "";


        stompRef.current.send('/pub/order/manual-call',  {}, JSON.stringify({posShopId : shopId, status:'pickup', posOrderNo : callNum}));

        setTimeout(() => {
            setCallMsg(null);
        }, 2000);
    }

    const onClickClear = () => {
        console.log('all cliear')
        stompRef.current.send('/pub/order/all-clear',  {}, JSON.stringify({posShopId : shopId, status:'all-clear'}));
        setCallList([]);
        console.log('all cliear end')
    }
    const complete = (index) => {
        let tempList = [...callList];
        const callNum = tempList[index];
        tempList.splice(index, 1);
        setCallList(tempList);

        stompRef.current.send('/pub/order/manual-call',  {}, JSON.stringify({posShopId : shopId, status:'complete', posOrderNo : callNum}));

    }

        let myInt;
    const cennectWs = () => {

        if(stompRef.current){
            stompRef.current.disconnect();
        }

        let stopmObj = Stomp.over(new SockJS(endpoint));

        stopmObj.debug = null;

        console.log("connecting ...")
        stopmObj.connect({}, function () {
            console.log('connected!')
            setNetworkStatus('enable');
            stompRef.current = stopmObj
            // setStomp(stopmObj);

        }, function () {
            // error
            // clearInterval(myInt)
            console.log("connect fail !")
            setNetworkStatus('disable');
            myInt = setTimeout(() => {
                console.log('retry!')
                cennectWs();
            }, 3000)
        })
    }

    useEffect(() => {

        const loginInfo = getLoginInfo();

        if (!loginInfo || !loginInfo.shopId) {
            router.push(`/register`);
        }

        setShopId(loginInfo.shopId);

        cennectWs();
    }, []);

    return <>
        <header className="commonHeader">
            <div className="navWrap">
                <h1 className="logo"><a href=""><i className="logoLeft"></i></a></h1>
                {/*<a className="btnSetting" style={{cursor:"pointer"}} onClick={()=>{router.push("/configure")}}>설정</a>*/}
            </div>
        </header>
        <div className="page">
            {networkStatus == 'disable' && <div className="dimmAlert">네트워크가 원활하지 않습니다.</div>}
            {callMsg && <div className="dimmAlert">{callMsg}번을 호출했습니다.</div>}
            <div className="manual-call">
                <div className="formCall">
                    <div className="inner">
                        <div className="tit">호출 번호 입력</div>
                        <div className="inputCall">
                            <input type="number" title="호출번호" ref={inputRef} placeholder="호출할 번호를 입력해주세요"/>
                                {/*<button className="btnClose" onClick={()=>{inputRef.current.value = ""}}>삭제</button>*/}
                        </div>
                        <button className="btnCall" type="button disabled" onClick={call}>호출하기</button>
                    </div>
                    <button className="btnBasicLine btnReset" type="button" onClick={onClickClear}>초기화</button>
                </div>
                <div className="callList">
                    <ul className="inner">
                        {callList.map((orderNumber, index)=>{
                            return <li key={orderNumber}><strong>{orderNumber}</strong>
                            <button type="button" onClick={()=>{complete(index)}}>완료</button>
                        </li>
                        })}
                    </ul>
                </div>
            </div>
        </div></>
}

import {createCookie, getLoginInfo} from "../service/login";
import {useRouter} from "next/router";
import {useEffect, useRef, useState} from "react";
import axios from "axios";
import {Scrollbars} from "react-custom-scrollbars";

export default function Register() {
    const router = useRouter();
    const loginInfo = getLoginInfo();
    const [selectedShop, setSelectedShop] = useState({});
    const [shopList, setShopList] = useState([]);
    const inputRef = useRef(null);
    const pwRef = useRef();
    const [pwValue, setPwValue] = useState(null);
    const apiUrl = `${process.env.API_DOMAIN}`;

    if (loginInfo && loginInfo.posShopId) {
        router.push(`/order-manager`);
    } else if (loginInfo && loginInfo.shopId) {
        router.push(`/number-gen`);
    }


    async function checkUser(shopId, password) {
        return await axios.post(apiUrl + "/user/check-user",{shopId : shopId, password : password});
    }

    const onClickReg = (e) => {
        e.preventDefault();

        if (!selectedShop.shopId) {
            alert('매장명을 입력하세요');
            return;
        }

        const pw = pwRef.current.value;
        if (!pw) {
            alert('패스워드를 입력해 주세요');
            return;
        }

        if (!selectedShop.sdmUseType || selectedShop.sdmUseType == 'pos' && !selectedShop.posShopId) {
            alert('주문호출 시스템 이용을 위해서 관리자에 문의하세요.')
            return;
        }

        checkUser(selectedShop.shopId, pw).then(res => {
            console.log('res : ', res.data)

            if(res.data) {
                createCookie("sdm_user", JSON.stringify({
                    shopId: selectedShop.shopId,
                    posShopId: selectedShop.posShopId,
                    sdmUseType: selectedShop.sdmUseType
                }));

                if (selectedShop.sdmUseType == 'manual') {
                    router.push(`/number-gen`);
                } else {
                    router.push(`/order-manager`);
                }
            }else{
                alert("패스워드를 확인해 주세요");
                return;
            }
        })

    }

    const onKeyUpInput = (e) => {
        const val = e.target.value;

        if(!val){
            return;
        }
        axios(`${process.env.API_DOMAIN}/shops/list?keyword=${val}`).then((res)=>{
            console.log("res.length : ", res.data.length)
            if(res.data && res.data.length > 0){
                setShopList(res.data)
            }else{
                setShopList([])
            }

        });
    }
    const onChangePw = (e) => {
        setPwValue(e.target.value);
    }

    const [pwLayer, setPwLayer] = useState(false);
    const onClickShop = (shop) => {
        console.log('shop', shop)
        setSelectedShop({...shop});
        setShopList([]);
        setPwLayer(true);
    }
    useEffect(()=>{

        document.querySelector('body').className= "regPage";

        return ()=>{
            document.querySelector('body').className = "";
        }
    },[])

    return <>
        <div className="page">
            <div className="logo" title="syncsign"></div>

            <div className="formReg">
                <h3>주문호출관리 시스템</h3>
                <form>
                    <legend>매장정보 입력</legend>
                    <div className="searchShop">
                        <div className="inputText">
                            <input type="text" className="compoEditText" defaultValue="" onKeyUp={onKeyUpInput} placeholder="매장이름을 입력해주세요" />
                            <span className="measure">매장이름을 입력해주세요</span>
                        </div>
                        <div className="address">{selectedShop.name}</div>
                        <button className="btnClose" type="button" onClick={()=>{setSelectedShop({})}}>삭제</button>
                    </div>
                    <div className={shopList.length > 0 ? "shopListWrp active" : "shopListWrp"}>
                        <ul className="shopList jsScroll">
                            <Scrollbars>
                            {shopList.map((shop, index)=>{
                                return <li key={index} onClick={()=>{onClickShop(shop)}}><strong>{shop.name}</strong><span>{shop.addr}</span></li>
                            })}
                            </Scrollbars>
                        </ul>
                    </div>
                    {/*<button type="submit" onClick={onClickReg} className={selectedShop.posShopId || selectedShop.sdmUseType=='manual' ? "start active" : "start"}>시작하기</button>*/}
                    <div className={pwLayer ? "checkPW active" : "checkPW"}>
                        <h2>비밀번호 확인</h2>
                        <div className="inputText">
                            <input type="password" title="패스워드 입력" ref={pwRef} onChange={onChangePw} autoComplete={"off"}/>
                        </div>
                        <button type="submit" onClick={onClickReg} className={pwValue && (selectedShop.posShopId || selectedShop.sdmUseType=='manual') ? "start active" : "start"}>시작하기</button>
                    </div>
                </form>
            </div>
        </div>
    </>
}

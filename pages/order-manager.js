import {getLoginInfo} from "../service/login";
import {useRouter} from "next/router";
import {useCallback, useEffect, useRef, useState} from "react";
import {Scrollbars} from "react-custom-scrollbars";
import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import axios from "axios";
import {getToday} from "../utils/odsUtils";
import {getAudioInfo} from "../service/ods.service";

export function OrderCard(props) {

    const [order, setOrder] = useState(props.order);
    const [orderDate] = useState(props.order?.orderDate);
    const [orderDateFormated, setOrderDateFormated] = useState(null);
    const [elapsedTime, setElapsedTime] = useState(0);
    const [elapsedMin, setElapsedMin] = useState(-1);
    const [delayTime] = useState(props.delayTime);
    const [currentStatus] = useState(props.order?.status);
    let timer = null;

    const nextStatus = (currentStatus) => {
        return currentStatus == 'new' ? 'accept' : currentStatus == 'accept' ? 'pickup' : currentStatus == 'pickup' ? 'complete' : currentStatus == 'complete' ? "pickup" : "";
    }

    const mainBtnText = useCallback((currentStatus) => {
        return currentStatus == 'new' ? '상품준비' : currentStatus == 'accept' ? '호출' : currentStatus == 'pickup'
            ? '픽업완료' : currentStatus == 'complete' ? "재호출" : "";
    }, []);

    useEffect(() => {
        setOrder(props.order);
    }, [props.order]);

    useEffect(() => {

        if (elapsedMin >= 60) {
            clearInterval(timer);
        }
    }, [elapsedMin]);

    useEffect(() => {

        let orderTime;
        if (orderDate) {
            orderTime = new Date(orderDate.replace(" ", 'T'));

            let orderD = (orderTime.getFullYear() + "").slice(2, 4) + "."
                + (orderTime.getMonth() + 1 < 10 ? "0" + (orderTime.getMonth() + 1) : orderTime.getMonth() + 1)
                + "." + (orderTime.getDate() < 10 ? "0" + orderTime.getDate() : orderTime.getDate());
            setOrderDateFormated(orderD);
        }


        if (orderDate && (currentStatus == 'new' || currentStatus == 'accept')) {

            timer = setInterval(() => {
                let currentTime = new Date();
                const timeInterval = currentTime.getTime() - orderTime.getTime();
                let sec = Math.floor(timeInterval / 1000);
                let secMod = sec % 60;
                let min = Math.floor(sec / 60);
                let minMod = min % 60;
                let hour = Math.floor(timeInterval / 1000 / 60 / 60);
                setElapsedMin(min);
                if (hour >= 1) {
                    setElapsedTime('1시간 이상');
                } else {
                    setElapsedTime((minMod < 10 ? '0' + min : min) + ":" + (secMod < 10 ? '0' + secMod : secMod));
                }

            }, 1000);

        }
    }, [currentStatus, orderDate]);

    useEffect(() => {
        return () => {
            clearInterval(timer);
        }
    }, []);

    return <div key={order?.orderId} className={order?.status==='cancel' ? 'orderCard cancel' : elapsedMin >= delayTime ? "orderCard delay" : "orderCard"}>
            <div className="time">
                <p className="call">{order?.orderTime}</p>
                {(order?.status == 'complete' || order?.status == 'cancel') && <p className="end">{orderDateFormated}</p>}
                <p className="after">{elapsedMin >= 0 ? elapsedTime+'경과' : ''}</p>
                 {order?.status != 'complete' && order?.status != 'cancel' &&
                <button className="btnCancel" type="button" onClick={()=>{props.changeNextStep(order, 'cancel')}}>주문취소</button>}
            </div>
            <div className="orderMenu">
                <div
                    className={elapsedMin >= delayTime ? "ordNum isDelay" : "ordNum"}>{order?.shopOrderNo}. {elapsedMin >= delayTime &&
                <i className="iconRound delay">지연</i>} {order?.status == 'cancel'? <em>취소주문</em> : ''}</div>
                <div
                    className="menuCount">메뉴 {order?.products?.length}개<span>영수증 : {order?.transactionNo}</span><span>POS : {order?.posDeviceId}</span>
                </div>
                <ul className="munuList">
                    {order && order.products.map((product, index1) => {
                        return <li key={index1}>{product.productName} {product.orderQty}개 {product.productMemo && '[' + product.productMemo + ']'}
                            {product.productOptions && product.productOptions.map((option, index2) => {
                                return <span key={index2}>+ {option.optionName}</span>
                            })}
                        </li>
                    })}
                    {order?.memo && <div className="ordComment">
                        <h5>요청사항</h5>
                        <p>{order?.memo}</p>
                    </div>}
                </ul>
            </div>
            <div className="funcBtn">
                <div className={order?.status == 'accept' ? "btnAlign duble" : "btnAlign"}>

                    {order?.status == 'accept' && <button className="btnBasicLine wSizeM" onClick={() => {
                        props.changeNextStep(order, 'complete')
                    }}>픽업완료</button>}


                    {order.status != 'cancel' &&
                    <button className="btnBasicPoint wSizeM" onClick={() => {
                        props.changeNextStep(order, nextStatus(order.status));
                    }}>{mainBtnText(order.status)}
                    </button>}
                </div>
            </div>
        </div>
}

export default function OrderManager() {

    const router = useRouter();
    const [shopId, setShopId] = useState(null);
    const posShopIdRef = useRef([]);
    const endpoint = `${process.env.SOCKET_DOMAIN}/ws`;
    const apiUrl = `${process.env.API_DOMAIN}`;

    const [topics, setTopics] = useState(null);

    const [activeTab, setActiveTab] = useState('new');
    const [orders, setOrders] = useState([]);
    const [completeOrders, setCompleteOrders] = useState([]);// 완료

    // config
    const [delayTime , setDelayTime] = useState(10);
    const [orderLimit, setOrderLimit] = useState(12);

    const [minOrderDate, setMinOrderDate] = useState(null);

    const [networkStatus, setNetworkStatus] = useState('try');
    const onGoingStatus = ['new', 'accept', 'pickup'];
    const onCompleteStatus = ['complete', 'cancel'];

    const newOrderRef = useRef([]);
    const completeOrderRef = useRef([]);
    const audioNoRef = useRef(null);
    const orderLimitRef = useRef();
    const stompRef = useRef();

    const [time, setTime] = useState();
    const [newIcon, setNewIcon] = useState(false);
    const [audioPlayNo, setAudioPlayNo] = useState(null);
    const [nextPageNo, setPageNo] = useState(1);

    const [isPageLoaded , setPageLoaded] = useState(false);
    const [isLoading, setLoading] = useState(false);
    const autoRefreshInt = useRef();

    async function fetchOrders(pageNo, status, sorting = "ORDER_ASC", orderLimitHour) {
        return await axios.get(apiUrl + "/orders?"
            + "posShopId=" + posShopIdRef.current
            + "&status=" + status
            + "&sorting=" +sorting
            + "&page=" + pageNo
            + "&orderLimitHour=" + (orderLimitHour ? orderLimitHour : ""));
    }

    async function fetchConfig() {
        return await axios.get(`${process.env.API_DOMAIN}/${posShopIdRef.current}/configure`);
    }

    const fetchAllData = () => {
        fetchOrders(-1, '', "ORDER_ASC", 12).then(data => {
            setOrders(data.data);
        });
    }

    const fetchCompleteData = (pageNo, orderLimitParam) => {
        setLoading(true);
        if (!orderLimitParam) {
            orderLimitParam = orderLimitRef.current;
        }
        fetchOrders(pageNo, 'complete',"ORDER_DESC", orderLimitParam).then(data => {
            if(pageNo>1){
                setCompleteOrders([...completeOrders, ...data.data]);
            } else {
                setCompleteOrders(data.data);
            }
            setLoading(false);
            setPageLoaded(true);
        });
    }

    const updateOrders = (item) => {

        let orderTemps = [...newOrderRef.current];
        let isNew = true;
        orderTemps = orderTemps.map((order)=>{
            if(order.posOrderNo == item.posOrderNo){
                isNew = false;
                return {...item};
            }else{
                return order;
            }
        })
        if(isNew){
            if (item.status == 'new') {
                setNewIcon(true);

                // sound play
                try {
                    if (audioNoRef.current && window.flutter_inappwebview) {
                        window.flutter_inappwebview.callHandler('playSound', audioNoRef.current);
                    }
                } catch (e) {
                    // alert(e)
                }
            }
            orderTemps.push(item);
        }

        setOrders(orderTemps);

         // if(item.status == 'pickup' ) {
         if(item.status == 'pickup' && item.rePickup == 'Y') {
             // removeCompleteOrder(item);
             fetchCompleteData(1);
         }
    }

    const updateCompleteOrders = (item) => {
        fetchCompleteData(1);
        removeOrder(item);
    }

    const removeOrder = (item) => {
        let orderTemps = [...newOrderRef.current];
        setOrders(orderTemps.filter((order) => order.orderId != item.orderId));
    }

    const getOrderList = useCallback((status) => {
        if (onGoingStatus.includes(status)) {
            return orders.filter((order) => order.status == status);
        } else {
            return completeOrderRef.current;
        }
    }, [orders, completeOrders]);

    const getCount = useCallback((status)=>{
        if(onGoingStatus.includes(status)){
            return orders.filter((order)=> order.status == status).length;
        }else{
            return completeOrders.length > 0 ? completeOrders[0].totalCount : 0;
        }
    }, [orders, completeOrders])


    const isDelay = () => {
        if(!minOrderDate){
            return false;
        }

        let currentTime = new Date();
        const od = minOrderDate.replace(" ", 'T')
        let orderTime = new Date(od);
        const timeInterval = currentTime.getTime() - orderTime.getTime();
        let sec = Math.floor(timeInterval / 1000);
        let min = Math.floor(sec / 60);

        return min >= delayTime;
    }

    const changeNextStep = (order, tobeStatus) => {

        if (networkStatus!='enable') {
            alert("서버 연결 끊김!");
            return;
        }

        let transOrder = {...order};
        transOrder.status = tobeStatus;
        if (tobeStatus == 'cancel') {
            transOrder.cancelDate = getToday();
        }

        if(tobeStatus=='pickup' && order.status=="complete"){
            transOrder.rePickup = "Y";
        }

        stompRef.current.send('/pub/order/status', {}, JSON.stringify(transOrder));
    }

    const changeTab = (tab) => {
         if(activeTab=='new' && tab!='new'){
             setNewIcon(false);
         }
         if (tab=='new'){
             setNewIcon(false);
         }

        if (tab == 'complete') {
            setNewIcon(false);
            fetchCompleteData(1);
        }

         setActiveTab(tab);

         // scrollTop = 0
        const scrollEl = document.querySelector(".scrollArea");
        const scrollChild = scrollEl.firstElementChild.firstElementChild;
        scrollChild.scrollTop = 0;

    }

    useEffect(() => {
        audioNoRef.current = audioPlayNo;
    }, [audioPlayNo]);

    useEffect(() => {
        orderLimitRef.current = orderLimit;
    }, [orderLimit]);

    useEffect(() => {

        if (networkStatus!='enable') {
            return;
        }

        fetchAllData();

        fetchConfig(shopId).then(data => {
            let orderLimitParam;
            for (let conf of data.data) {
                if (conf.configKey === 'delayMin') {
                    setDelayTime(conf.configValue)
                } else if (conf.configKey === 'orderLimit') {
                    setOrderLimit(conf.configValue);
                    orderLimitParam = conf.configValue;
                } else if (conf.configKey === 'alarm') {
                    let audioSrc = null;
                    if (conf.configValue) {
                        audioSrc = getAudioInfo(conf.configValue.replace(".wav", "")).no;
                        setAudioPlayNo(audioSrc);
                    }else{
                        setAudioPlayNo(null);
                    }

                }
            }

            fetchCompleteData(1, orderLimitParam);
        });

        // auto refresh
        clearInterval(autoRefreshInt.current);
        autoRefreshInt.current = setInterval(() => {
            fetchAllData();
            fetchCompleteData(1);
        }, 1000 * 60 * 60);
        // }, 1000 * 30);

    }, [networkStatus]);

    useEffect(() => {

        newOrderRef.current = orders;

        // 최소주문시간 설정 :: 지연 아이폰 표시를 위해서
        let minValue = null;
        orders.map((d)=>{
            if(d.status != 'accept'){
                return;
            }
            if(!minValue){
                minValue = d.orderDate;
            }else if(minValue > d.orderDate){
                minValue = d.orderDate;
            }
        })

        setMinOrderDate(minValue);

    }, [orders]);


    useEffect(() => {
        completeOrderRef.current = completeOrders;
    }, [completeOrders]);


    useEffect(() => {

        const loginInfo = getLoginInfo();

        if (!loginInfo || !loginInfo.posShopId) {
            router.push(`/register`);
        }
        posShopIdRef.current = loginInfo.posShopId;
        // setPosShopId(loginInfo.posShopId);
        setShopId(loginInfo.shopId);
        const topic = `/sub/shop_${loginInfo.posShopId}/manager`;
        setTopics(topic);


        cennectWs(topic);
        // for delay display
        const timeInt = setInterval(()=>{
            setTime(new Date());
        }, 1000)

        return () => {
            // console.log("disconnect !!!", stompRef.current);
            if (stompRef.current) {
                stompRef.current.disconnect();

            }
            clearInterval(timeInt);
            clearInterval(autoRefreshInt.current);
        }
    }, []);

    let myInt;
    const cennectWs = (topic_param) => {

        if(!topic_param){
            topic_param = topics;
        }
        if(stompRef.current){
            stompRef.current.disconnect();
        }

        let stopmObj = Stomp.over(new SockJS(endpoint));

        stopmObj.debug = null;

        console.log("connecting ...")
        stopmObj.connect({}, function () {
            console.log('connected!');
            // console.log('topics : '+ topic_param)

            setNetworkStatus('enable');
            stopmObj.subscribe(topic_param, function (order) {
                // console.log("order",order)
                const content = JSON.parse(order.body);
                const status = content.orderInfo.status;
                if (!content.orderInfo) {
                    return;
                } else if (onGoingStatus.includes(status)) {
                    updateOrders(content.orderInfo);
                }else if (status === 'complete' || status === 'cancel') {
                    updateCompleteOrders(content.orderInfo);
                }
            });
            stompRef.current = stopmObj
            // setStomp(stopmObj);

        }, function () {
            // error
            // clearInterval(myInt)
            console.log("connect fail !")
            setNetworkStatus('disable');
            myInt = setTimeout(() => {
                console.log('retry!')
                cennectWs();
            }, 3000)
        })
    }


    const scrollRef = useRef();

    const onscroll = ({ target: { scrollTop } }) => {

        const scrollHeight = scrollRef.current.getScrollHeight();
        const clientHeight = scrollRef.current.getClientHeight();
        const scrollBottom = scrollTop + clientHeight;
        // console.log(`${scrollHeight} | ${scrollBottom}`);

        if(activeTab=='complete' && !isLoading && (scrollHeight == Math.ceil(scrollBottom))){
            setLoading(true);
            const nextPage = nextPageNo + 1;
            setPageNo(nextPage);
            fetchCompleteData(nextPage);
        }

    }
    const onScrollStop =(e) => {
    }
    return <>
        <a href="#content" className="skip">Skip to Content</a>
        <header className="commonHeader" style={ isPageLoaded ?  null :  { visibility:"hidden"}}>
            <div className="navWrap">
                <h1 className="logo"><a href=""><i className="logoLeft"></i></a></h1>
                <h2 className="title">{activeTab == 'new' ? "신규주문" : activeTab == 'accept' ? "상품준비중" : activeTab == 'pickup' ? "핍업대기중" : "픽업완료"}</h2>
                <a className="btnSetting" style={{cursor:"pointer"}} onClick={()=>{router.push("/configure")}}>설정</a>
            </div>
        </header>
        <div className="page" style={ isPageLoaded ?  null :  { visibility:"hidden"}}>
            {networkStatus == 'disable' && <div className="dimmAlert">네트워크가 원활하지 않습니다.</div>}
            <div className="orderLayout">
                <div className="summary">
                    <div className={activeTab == "new" ? "summaryTab active" : "summaryTab"} onClick={() => {
                        changeTab("new");
                    }}>
                        <h4>신규주문</h4>
                        <p className="count">{getCount('new')}</p>
                        {newIcon && getCount('new') > 0 && <i className="iconRound new">NEW</i>}
                    </div>
                    <div className={activeTab == "accept" ? "summaryTab active" : "summaryTab"} onClick={() => {
                        changeTab("accept")
                    }}>
                        <h4>상품준비중</h4>
                        <p className="count">{getCount('accept')}</p>
                        {isDelay() && <i className="iconRound delay">지연</i>}
                    </div>
                    <div className={activeTab == "pickup" ? "summaryTab active" : "summaryTab"} onClick={() => {
                        changeTab("pickup")
                    }}>
                        <h4>픽업대기중</h4>
                        <p className="count">{getCount('pickup')}</p>
                    </div>
                    <div className={activeTab == "complete" ? "summaryTab active" : "summaryTab"} onClick={() => {
                        changeTab("complete")
                    }}>
                        <h4>픽업완료</h4>
                        <p className="count">{getCount('complete')}</p>
                    </div>
                </div>
                <div className="scrollArea jsScroll">
                    {(getCount(activeTab)) == 0 ?
                        <div className="orderNone">
                            <p>해당 상태의 주문이 없습니다.</p>
                        </div> :
                        <Scrollbars onScroll = {onscroll} onScrollStop = {onScrollStop} ref={scrollRef}>
                            <div className="orderList">
                                {getOrderList(activeTab).map((order, index) => <OrderCard order={order} key={order.orderId}
                                                      changeNextStep={changeNextStep} delayTime={delayTime}/>
                                )}
                            </div>
                        </Scrollbars>
                    }
                </div>
            </div>
        </div>
    </>
}

import {useRouter} from "next/router";
import {useEffect, useRef, useState} from "react";
import axios from "axios";
import useInput from "../hook/useInput";
import {Scrollbars} from "react-custom-scrollbars";
import {getLoginInfo} from "../service/login";
import {getAudioInfo} from "../service/ods.service";
export default function Configure() {

    const router = useRouter();

    const [delayMin, delayHandler, setDelayMin] = useInput(10);
    const [orderLimit, orderLimitHandler, setOrderLimit] = useInput(12);

    const [alarmAudio, setAlarmAudio] = useState(getAudioInfo('Doorbell1'));
    const [showAudioCombo, setAudioCombo] = useState(false);
    const [useAlarm, setUseAlarm] = useState(true);

    const [boardAlarmAudio, setBoardAlarmAudio] = useState(getAudioInfo('Doorbell1'));
    const [showBoardAudioCombo, setBoardAudioCombo] = useState(false);
    const [useBoardAlarm, setUseBoardAlarm] = useState(true);

    const [shopId, setShopId] = useState(null);



    const goMain = () => {
       router.push(`/register`);
       // document.location.href="/order-manager";
    }

    const cancel = () => {
        router.push(`/register`);
        // document.location.href="/order-manager";
    }
    const save = () => {

        // const delayMin = delayRef.current.value;
        // const orderLimit = completeOrderRef.current.value;

        axios.post(`${process.env.API_DOMAIN}/${shopId}/configure`,
            [{configKey: 'delayMin', configValue: delayMin}
                , {configKey: 'orderLimit', configValue: orderLimit}
                , {configKey: 'alarm', configValue: alarmAudio.fileName}
                , {configKey: 'boardAlarm', configValue: boardAlarmAudio.fileName}])
            .then(res => {
                router.push(`/register`);
                // document.location.href="/order-manager";
            })
    }

    const changeAudio = (ado)=>{

        setAlarmAudio(ado);

        const audioObj = new Audio(ado.src);
        audioObj.play().then(() => {
            // console.log("play")
        }).catch((e) => {
            console.log(e)
        })

        setAudioCombo(false);
    }
    const changeBoardAudio = (ado)=>{

        setBoardAlarmAudio(ado);

        const audioObj = new Audio(ado.src);
        audioObj.play().then(() => {
            // console.log("play")
        }).catch((e) => {
            console.log(e)
        })

        setBoardAudioCombo(false);
    }
    const onClickAlarmSet = (e) => {
        if(!e.target.checked){
            setUseAlarm(false);
            setAlarmAudio( getAudioInfo('none'));
            setAudioCombo(false);
        }else{
            setUseAlarm(true);
        }
    }

    const onClickBoardAlarmSet = (e) => {
        if (!e.target.checked) {
            setUseBoardAlarm(false);
            setBoardAlarmAudio(getAudioInfo('none'));
            setBoardAudioCombo(false);
        } else {
            setUseBoardAlarm(true);
        }
    }


    useEffect(() => {

        const loginInfo = getLoginInfo();

        console.log("loginInfo", loginInfo)
        if(loginInfo && loginInfo.shopId) {
            setShopId(loginInfo.shopId);
            axios.get(`${process.env.API_DOMAIN}/${loginInfo.shopId}/configure`)
                .then(res => {

                    console.log("res.data", res.data)

                            for (let conf of res.data) {
                                if (conf.configKey === 'delayMin') {
                                    setDelayMin(conf.configValue)
                                }  else if (conf.configKey === 'orderLimit') {
                                    setOrderLimit(conf.configValue);
                                } else if (conf.configKey === 'alarm') {
                                    if (!conf.configValue) {
                                        setAlarmAudio(getAudioInfo('none'));
                                        setUseAlarm(false);
                                        setAudioCombo(false);
                                    } else {
                                        const data = getAudioInfo(conf.configValue.replace(".wav", ""));
                                        console.log(data)
                                        setAlarmAudio(data);
                                    }
                                } else if (conf.configKey === 'boardAlarm') {
                                    if (!conf.configValue) {
                                        setBoardAlarmAudio(getAudioInfo('none'));
                                        setUseBoardAlarm(false);
                                        setBoardAudioCombo(false);
                                    } else {
                                        setBoardAlarmAudio(getAudioInfo(conf.configValue.replace(".wav", "")));
                                    }

                                }
                            }
                });
        }
        return ()=>{
        }
    }, []);


    return <>
        <header className="commonHeader">
            <div className="navWrap">
                <h1 className="logo"><a href=""><i className="logoLeft"></i></a></h1>
                <h2 className="title">환경설정</h2>
            </div>
        </header>
        <div className="page">
            <div className="breadcrumb">
                <div className="now">
                    <a onClick={()=>{goMain()}} className="historyBack">이전 페이지</a>
                    <h3>환경설정</h3>
                </div>
                <div className="funcBtns">
                    <button type="button" onClick={cancel}>취소</button>
                    <button type="button" onClick={save} className="active">저장</button>
                </div>
            </div>

            <div className="settings">
                <Scrollbars>
                <div className="section">
                    <h4>지연시간 설정</h4>
                    <p className="comment">신규주문/상품준비중 주문의 지연이 처리되는 시간</p>
                    <div className="timeset">
                        <div className="inputBasic"><input type="text" title="시간 입력" value={delayMin} onChange={delayHandler}/></div>
                        <span>분</span>
                    </div>
                </div>
                <div className="section">
                    <h4>완료된 주문 조회 제한</h4>
                    <p className="comment">설정한 시간 이내의 주문건만 조회됨</p>
                    <div className="timeset">
                        <div className="inputBasic"><input type="text" title="시간 입력" value={orderLimit} onChange={orderLimitHandler}/></div>
                        <span>시간</span>
                    </div>
                </div>
                <div className="section">
                    <div className="grp">
                        <h4>SDM 알림음 설정</h4>
                        <div className="moveCheck"><label><input type="checkbox" checked={useAlarm} onClick={onClickAlarmSet} /><span></span></label></div>
                    </div>
                    <div className={useAlarm ? "selectBox" : "selectBox disabled"}>
                        <div className="selectBoxTop" onClick={()=>{useAlarm && setAudioCombo(true)}}>
                            <input type="text" className="selected" value={alarmAudio.name}/>
                                <span className="selectArrow"></span>
                        </div>
                        {showAudioCombo && <div className="selectOptions jsScroll" style={{zIndex:10}}>
                            <Scrollbars>
                            <div className="scrollwrap" style={{display:"block"}}>
                                <span className="selectOption" onClick={()=>{changeAudio({src : '/audio/Doorbell1.wav', name:'알림음1', fileName : 'Doorbell1.wav'})}}>알림음1</span>
                                <span className="selectOption" onClick={()=>{changeAudio({src : '/audio/Doorbell2.wav', name:'알림음2', fileName : 'Doorbell2.wav'})}}>알림음2</span>
                                <span className="selectOption" onClick={()=>{changeAudio({src : '/audio/Doorbell3.wav', name:'알림음3', fileName : 'Doorbell3.wav'})}}>알림음3</span>
                                <span className="selectOption" onClick={()=>{changeAudio({src : '/audio/Doorbell4.wav', name:'알림음4', fileName : 'Doorbell4.wav'})}}>알림음4</span>
                            </div>
                            </Scrollbars>
                        </div>}
                    </div>
                </div>
                <div className="section">
                    <div className="grp">
                        <h4>메뉴보드 알림음 설정</h4>
                        <div className="moveCheck"><label><input type="checkbox" checked={useBoardAlarm} onClick={onClickBoardAlarmSet} /><span></span></label></div>
                    </div>
                    <div className={useBoardAlarm ? "selectBox" : "selectBox disabled"}>
                        <div className="selectBoxTop" onClick={()=>{useBoardAlarm && setBoardAudioCombo(true)}}>
                            <input type="text" className="selected" value={boardAlarmAudio.name}/>
                                <span className="selectArrow"></span>
                        </div>
                        {showBoardAudioCombo && <div className="selectOptions jsScroll" style={{zIndex:10}}>
                            <Scrollbars>
                            <div className="scrollwrap" style={{display:"block"}}>
                                <span className="selectOption" onClick={()=>{changeBoardAudio({src : '/audio/Doorbell1.wav', name:'알림음1', fileName : 'Doorbell1.wav'})}}>알림음1</span>
                                <span className="selectOption" onClick={()=>{changeBoardAudio({src : '/audio/Doorbell2.wav', name:'알림음2', fileName : 'Doorbell2.wav'})}}>알림음2</span>
                                <span className="selectOption" onClick={()=>{changeBoardAudio({src : '/audio/Doorbell3.wav', name:'알림음3', fileName : 'Doorbell3.wav'})}}>알림음3</span>
                                <span className="selectOption" onClick={()=>{changeBoardAudio({src : '/audio/Doorbell4.wav', name:'알림음4', fileName : 'Doorbell4.wav'})}}>알림음4</span>
                            </div>
                            </Scrollbars>
                        </div>}
                    </div>
                </div>
                </Scrollbars>
            </div>
        </div>
    </>
}

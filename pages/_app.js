// import '../styles/globals.css'
import '../public/css/common.css';
import '../public/css/content.css';
import {useEffect} from "react";

function MyApp({ Component, pageProps }) {

  useEffect(() => {
    const nextEl = document.getElementById('__next');
    nextEl.style = 'height:calc(100% - 0px)';

  }, [])

  return <Component {...pageProps} />
}

export default MyApp

## Local 서버

```
npm install
```
```
npm run local
```
###

---
## 개발 서버

##### 52.141.33.236 (port : 3003)
URL : [https://dev-ods.el-bigs.com](https://dev-ods.el-bigs.com)

```

$ /home/elbigs/syncsign/sdm-front

$ yarn install
$ start-pm.sh

# 재실행
$ restart-pm.sh
```
###

---
## 운영 서버

##### 52.231.222.169 (port : 3003)
URL : [https://sdm.el-bigs.com](https://sdm.el-bigs.com)

```
$ cd /home/s2admin/syncsign-mboard/sdm-front
$ restart-pm.sh
```
